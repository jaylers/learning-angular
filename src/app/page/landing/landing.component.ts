import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http'

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private http: Http) {
    let numList = [1, 2, 3, 4, 5, 6];
    numList.forEach(element => {
      console.log(element);
    });

    let nextNumList = numList.filter(num => num > 3);
    console.log(nextNumList);
  }

  ngOnInit() {
  }

  status = 'Unknow';
  IsConnected() {
    this.status = 'Checking ...';
    this.http.get("http://128.199.97.89/app/connection/connection.php").subscribe(Response => {
      console.log(Response);
      this.status = Response.toString();
    })
  }

}
