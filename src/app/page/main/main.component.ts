import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  title = 'Welcome';
  app_name = 'Jaylerr';
  name_title = ["Mr.", "Miss", "Mrs."];
  coffee = [{ id: "001", name: "Americano" }, { id: "002", name: "Capuchino" }, { id: "003", name: "Late" }, { id: "004", name: "nomm" }];

  showInput: boolean = false;

  testCkick() {
    this.title = "Good bye!";
    this.showInput = this.showInput ? false : true;
    console.log('Clicked');
  }

  testDblCkick() {
    this.title = "Welcome!";
    console.log('Clicked');
  }

  onKeyUp() {
    this.title = "What do you want?";
    console.log("key up");
  }

  onKeyDown(event) {
    this.title = event.target.value;
  }

}
